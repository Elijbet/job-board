import React, { useState, useEffect } from "react";
import './App.css';
import Dropdown from './Dropdown';
import axios from 'axios';
import { skills } from './skills'
// mock data:
// import { jobs } from './jobs'


function App() {

  const [jobPostings, setJobPostings] = useState(0)
  const [finalFound, setFinalFound] = useState('')
  const [list, setList] = useState(skills);

  useEffect(async () => { 
    const result = await axios('https://api.jsonbin.io/b/5f975ab4076e516c36fbc87f',);
    setJobPostings(result.data.job_postings);
  }, []);

  // Toggle item on to select it and show the component which allows to rate the skill. 
  // Rate the skill on a scale of 1 to 5. 
  // Toggle off to deselect and reset the rating to 1.
  // Once selected and rated, click on button below to generate job listings for the selections listed highest score to lowest.
  // Regenerate if skills are modified.

  const toggleItem = (id) => {
    
    const updatedList = [...list]

    let count = 1
    updatedList.filter(item => item.selected ? count++ : count)

    const itemIndex = updatedList.findIndex(item => item.id === id)

    updatedList[itemIndex].selected = !updatedList[itemIndex].selected
    if(count > 10){ updatedList[itemIndex].selected = false } 
    
    if(!updatedList[itemIndex].selected){
      updatedList[itemIndex].rating = 1
    }
    setList(updatedList);

  }

  const incrementRating = (id, e) => {

    e.stopPropagation()

    const updatedList = [...list]
    const itemIndex = updatedList.findIndex(item => item.id === id)

    if(updatedList[itemIndex].rating >= 1 && updatedList[itemIndex].rating < 5){
      updatedList[itemIndex].rating = updatedList[itemIndex].rating + 1
    }

    setList(updatedList);
    
  }

  const decrementRating = (id, e) => {

    e.stopPropagation()

    const updatedList = [...list]
    const itemIndex = updatedList.findIndex(item => item.id === id)

    if(updatedList[itemIndex].rating > 1 && updatedList[itemIndex].rating <= 5){
      updatedList[itemIndex].rating = updatedList[itemIndex].rating - 1
    }

    setList(updatedList);

  }

  const generate = () => {
    let filteredList = {}
    list.forEach(skill => {
      if(skill.selected === true){
        filteredList[skill.title] = skill.rating
      }
    })
    const matchedJobs = []
    jobPostings.forEach(job => {
      let jobSkills = job.skills.split(',')
      let jobScore = 0
      for(let i = 0; i < jobSkills.length; i++){
        if(filteredList.hasOwnProperty(jobSkills[i])){
          jobScore += filteredList[jobSkills[i]];
        }
      }
      if(jobScore > 0){
        job.matchingScore = jobScore
        matchedJobs.push(job)
      }
    })
    matchedJobs.sort((a, b) => b.matchingScore - a.matchingScore);
    setFinalFound({finalFound: matchedJobs})
  }
  return (
    <div className="App">
      <header className="App-header">
        <Dropdown  
          list = {list}
          toggleItem = {toggleItem}
          incrementRating = {incrementRating}
          decrementRating = {decrementRating}
        />
        <button 
          className="generate-button" 
          onClick={() => generate()}
        >
          Generate listings based on my skillset.
        </button>
        {finalFound && (
          <div>
            {finalFound.finalFound.map(item => (
              <button key={item.id} className = "answer-button">
                <div><b>Title: </b>{item.title}</div>
                <div><b>Company: </b>{item.company}</div>
                <div><b>Skills: </b>{item.skills}</div>
                <div><b>Matching Score: </b>{item.matchingScore}</div>
              </button>
            ))}
          </div>
        )}
      </header>
    </div>
  );
}

export default App;
