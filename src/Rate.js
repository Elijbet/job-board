import React, { useState } from "react";
import './App.css';

function Rate({ item, incrementRating, decrementRating }){

	return (
		<div className = 'dd-rate-button'>
	    <div onClick = { (e) => decrementRating(item.id, e) }> - </div>
	    <div className="count">{ item.rating }</div>
			<div onClick = { (e) => incrementRating(item.id, e) }> + </div>
	  </div>
	)

}

export default Rate