export const skills = [
  {
      id: 0,
      title: 'PHP',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 1,
      title: 'Vue.js',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 2,
      title: 'AWS',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 3,
      title: 'Javascript',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 4,
      title: 'REST',
      selected: false,
      key: 'skill',
      rating: 1  
  },
  {
      id: 5,
      title: 'Java',
      selected: false,
      key: 'skill',
      rating: 1             
  },
  {
      id: 6,
      title: 'Angular',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 7,
      title: 'GCP',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 8,
      title: 'HTML',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 9,
      title: 'InDesign',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 10,
      title: 'CSS',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 11,
      title: 'Photoshop',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 12,
      title: 'Figma',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 13,
      title: 'java',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 14,
      title: 'Linux',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 15,
      title: 'Spring Boot',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 16,
      title: 'Github',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 17,
      title: 'JavaScript',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 18,
      title: 'jQuery',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 19,
      title: 'Java',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 20,
      title: 'JSON',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 21,
      title: 'NoSQL',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 22,
      title: 'Lucene',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 23,
      title: 'Tableau',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 24,
      title: 'PowerBI',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 25,
      title: 'Tensorflow',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 26,
      title: 'Scala',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 27,
      title: 'R',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 28,
      title: 'React',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 29,
      title: 'React Native',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 30,
      title: 'Azure',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 31,
      title: 'Windows',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 32,
      title: 'GraphQL',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 33,
      title: 'JQuery',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 34,
      title: 'C++',
      selected: false,
      key: 'skill',
      rating: 1
  },
  {
      id: 35,
      title: 'C#',
      selected: false,
      key: 'skill',
      rating: 1
  },
];