import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleUp, faAngleDown, faCheck } from "@fortawesome/free-solid-svg-icons";
import './App.css';
import Rate from './Rate';

function Dropdown({ title, list, toggleItem, incrementRating, decrementRating }) {
  const [isListOpen, setIsListOpen] = useState(false);
  const toggleList = () => setIsListOpen(!isListOpen);

  return (

    <div className="dd-wrapper">
      <button
        type="button"
        className="dd-header"
        onClick={toggleList}
      >
        <div className="dd-header-title"><h3>Select and rate 10 skills.</h3></div>
        {isListOpen
          ? <FontAwesomeIcon icon={faAngleUp} size="2x" />
          : <FontAwesomeIcon icon={faAngleDown} size="2x" />}
      </button>
      {isListOpen && (
        <div
          role="list"
          className="dd-list"
        >
          {list.map((item) => (
            <button
              type="button"
              className="dd-list-item"
              key={item.id}
              onClick={() => toggleItem(item.id)}
            >
              {item.title}
              {' '}
              {item.selected && <FontAwesomeIcon icon={faCheck} size="1x" />}
              {item.selected ? (
              <Rate
                item = {item}           
                incrementRating = {incrementRating}
                decrementRating = {decrementRating}
              />):null
              }
            </button>
          ))}
        </div>
      )}
    </div>
  )
}

export default Dropdown