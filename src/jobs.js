export const jobs = [
  {"id":1029,"title":"Software Engineer","company":"Phonet","skills":"PHP,Vue.js,AWS,Javascript,REST"},
  {"id":1030,"title":"Senior Backend Developer","company":"Geekval","skills":"Java,C#,C++,Angular,GCP"},
  {"id":1031,"title":"UI Developer","company":"Standlabs","skills":"HTML,CSS,InDesign,Photoshop,Figma"},
  {"id":1032,"title":"Software Development Engineer","company":"HSoftware","skills":"Java,Linux,REST,Spring Boot,Github"},
  {"id":1033,"title":"Fullstack Software Engineer","company":"Commer","skills":"HTML,CSS,JavaScript,jQuery,Java,JSON"},
  {"id":1034,"title":"Business Intelligence Engineer","company":"Serverst","skills":"NoSQL,Lucene,Tableau,PowerBI,Tensorflow,Scala,R"},
  {"id":1035,"title":"Software Engineer","company":"Backchip","skills":"React,React Native,PHP,Azure,Windows"},
  {"id":1036,"title":"Senior Software Engineer","company":"Twire","skills":"PHP,React,AWS,GraphQL,JavaScript,JQuery,HTML,CSS"}
]

